<?php

class MenuController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function standardAction()
    {
        $this->_helper->viewRenderer->setResponseSegment('selectMenu');
        $test = array();
        for($i=500; $i > 0; $i--)
            $test[$i] = 'Pack #' . $i;
        $this->view->selectMenu = $test;
    }

    public function linksAction()
    {
        $this->_helper->viewRenderer->setResponseSegment('links');
        $this->view->externalLinks = array(
                'osu!' => 'http://osu.ppy.sh',
                'google' => 'http://google.com');
    }
}

