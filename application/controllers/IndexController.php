<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->_helper->getHelper('Redirector')->gotoRoute(array('pack' => 'latest'),'standard_route');
    }


}

