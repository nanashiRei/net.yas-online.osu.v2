<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initHead()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');

        $view->headTitle('osu!Mirror')
            ->setSeparator(' :: ')
            ->setDefaultAttachOrder(Zend_View_Helper_Placeholder_Container::PREPEND);

        $view->headLink()
            ->headlink(array('rel' => 'icon',
                    'href' => $view->baseUrl('favicon.ico'),
                    'type' => 'image/x-icon'),
                    'PREPEND')
            ->appendStylesheet($view->baseUrl('osuv2.css'));
        
        $view->headScript()
            ->appendFile($view->baseUrl('js/jquery-1.7.2.min.js'))
            ->appendFile($view->baseUrl('js/jquery-ui-1.8.20.custom.min.js'))
            ->appendFile($view->baseUrl('js/jquery.kinetic.js'))
            ->appendFile($view->baseUrl('js/jquery.cookie.js'))
            ->appendFile($view->baseUrl('js/main.js'))
            ->appendFile($view->baseUrl('js/html5shiv.js'),'text/javascript',array('conditional' => 'lt IE 9'));

        $view->headMeta()
            ->appendName('robots','index,follow');
    }
    
    protected function _initRoutes()
    {
        $this->bootstrap('frontController');
        $router = $this->getResource('frontController')->getRouter();
        $standardRoute = new Zend_Controller_Router_Route('p/:pack',
                array(  'module' => 'default', 
                        'controller' => 'standard',
                        'action' => 'view',
                        'pack' => 'latest'));
        $themedRoute = new Zend_Controller_Router_Route('t/:theme/:pack',
                array(  'module' => 'default',
                        'controller' => 'themed',
                        'action' => 'autoselect',
                        'theme' => '',
                        'pack' => ''));
        $namedRoute = new Zend_Controller_Router_Route('n/:theme/:pack',
                array(  'module' => 'default',
                        'controller' => 'named',
                        'action' => 'autoselect',
                        'theme' => '',
                        'pack' => ''));
        $mapRoute = new Zend_Controller_Router_Route('m/:action/:mapid',
                array(  'module' => 'default',
                        'controller' => 'map',
                        'action' => 'browse',
                        'mapid' => '0'));
        $router->addRoute('standard_route',$standardRoute)
                ->addRoute('themed_route',$themedRoute)
                ->addRoute('named_route',$namedRoute)
                ->addRoute('map_route',$mapRoute);
    }
}

