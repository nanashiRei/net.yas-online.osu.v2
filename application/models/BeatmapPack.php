<?php

class Application_Model_BeatmapPack
{
    protected $_id;
    protected $_added;
    protected $_packname;
    protected $_filesize;
    protected $_filename;
    protected $_downloads;
    protected $_path;
    protected $_themename;
    protected $_numerical;
    
    protected $_mapper;
    
    public function __construct (array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function setOptions (array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
    }
    
    public function __get ($name)
    {
        $method = 'get' . ucfirst($name);
        if ('mapper' == $name || ! method_exists($this, $method)) {
            throw new Exception(
                    "Ungueltige Getter-Methode fuer Eigenschaft '$method'");
        }
        return $this->$method();
    }
    
    public function __set ($name, $value)
    {
        $method = 'set' . ucfirst($name);
        if ('mapper' == $name || ! method_exists($this, $method)) {
            throw new Exception(
                    "Ungueltige Setter-Methode fuer Eigenschaft '$name'");
        }
        return $this->$method($value);
    }
    
    public function getMapper ()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new Application_Model_BeatmapPackMapper();
        }
        return $this->_mapper;
    }
    
    public function setMapper ($mapper)
    {
        $this->_mapper = $mapper;
        return $this;
    }
	/**
     * @return the $_id
     */
    public function getId ()
    {
        return $this->_id;
    }

	/**
     * @return the $_added
     */
    public function getAdded ()
    {
        return $this->_added;
    }

	/**
     * @return the $_packname
     */
    public function getPackname ()
    {
        return $this->_packname;
    }

	/**
     * @return the $_filesize
     */
    public function getFilesize ()
    {
        return $this->_filesize;
    }

	/**
     * @return the $_filename
     */
    public function getFilename ()
    {
        return $this->_filename;
    }

	/**
     * @return the $_downloads
     */
    public function getDownloads ()
    {
        return $this->_downloads;
    }

	/**
     * @return the $_path
     */
    public function getPath ()
    {
        return $this->_path;
    }

	/**
     * @return the $_themename
     */
    public function getThemename ()
    {
        return $this->_themename;
    }

	/**
     * @return the $_numerical
     */
    public function getNumerical ()
    {
        return $this->_numerical;
    }

	/**
     * @param field_type $_id
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
    }

	/**
     * @param field_type $_added
     */
    public function setAdded ($_added)
    {
        $this->_added = $_added;
    }

	/**
     * @param field_type $_packname
     */
    public function setPackname ($_packname)
    {
        $this->_packname = $_packname;
    }

	/**
     * @param field_type $_filesize
     */
    public function setFilesize ($_filesize)
    {
        $this->_filesize = $_filesize;
    }

	/**
     * @param field_type $_filename
     */
    public function setFilename ($_filename)
    {
        $this->_filename = $_filename;
    }

	/**
     * @param field_type $_downloads
     */
    public function setDownloads ($_downloads)
    {
        $this->_downloads = $_downloads;
    }

	/**
     * @param field_type $_path
     */
    public function setPath ($_path)
    {
        $this->_path = $_path;
    }

	/**
     * @param field_type $_themename
     */
    public function setThemename ($_themename)
    {
        $this->_themename = $_themename;
    }

	/**
     * @param field_type $_numerical
     */
    public function setNumerical ($_numerical)
    {
        $this->_numerical = $_numerical;
    }


}

