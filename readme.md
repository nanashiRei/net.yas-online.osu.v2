osu!Mirror
==========

Primäre Ziele
-------------

*   OOP basierter code um einfaches erweitern und warten zu ermöglichen
*   Durch Verwendung des Zend Framework (v1.11.11) eine sichere und schnelle basis
    für ein API schaffen.
*   Verwendung von AJAX für in-site laden von Inhalten
*   Lokalisierung
*   Besseres System zum Merken der bereits runtergeladenen Map/Packs
*   Caching
*   HTML5

Sonstiges Ziele
---------------

Diese Zielen werden, wenn überhaupt, als letztes bearbeitet

*   Testumgebung benutzten
*   PHP Unit verwenden
*   Keine SQL-Queries selber schreiben
*   User Upload (Maps)

Team
----

An diesem Projekt werden beteiligt sein:

*   [David Marner](http://development.yas-online.net "David Marner") (nanashiRei) Projekt Leiter
*   [Sven Straubel](https://bitbucket.org/Ezoda "Sven Straubel") (Ezoda) Entwicklungshelfer

Dieses Projekt wird nicht Open-Source sein und unter ein Allgemeines, privates, Urheberrecht fallen. 