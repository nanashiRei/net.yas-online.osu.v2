<?php

class OsuMirror_Plugin_SidebarRight extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)    
    {

        if(!$request->isXmlHttpRequest()) {
            $front = Zend_Controller_Front::getInstance();
    
            if (!$front->hasPlugin('Zend_Controller_Plugin_ActionStack')) {
                $actionStack = new Zend_Controller_Plugin_ActionStack();
                $front->registerPlugin($actionStack, 97);
            } else {
                $actionStack = $front->getPlugin('Zend_Controller_Plugin_ActionStack');
            }
    
            $menuAction = clone($request);
            $menuAction->setActionName('links')
                        ->setControllerName('menu');
            $actionStack->pushStack($menuAction);
        }
    }
}