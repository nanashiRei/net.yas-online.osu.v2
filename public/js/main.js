selectMenuMoving = false;
$(document).ready(function(){
		
	var smenu = $('#select-menu');
	
	smenu
		.kinetic({
			x: false,
			maxvelocity: 250,
			slowdown: 0.95,
			moved: function(){ selectMenuMoving = true; },
			stopped: function(){ selectMenuMoving = false; }})
		.find('a')
			.click(cancelClick);
	
	var jumpSelect = location.pathname.match(/\/(?:p|t|n)\/(\d+)/i);
	if(jumpSelect){
		if($.cookie('jselpos')){
			smenu.scrollTop($.cookie('jselpos'));
		}
		var item = $('#select-menu ul li #select-item-' + jumpSelect[1]);
		var offsetTop = item.position().top - 35 + smenu.scrollTop();
		item
			.addClass('blue')
			.removeClass('red');
		smenu
			.animate({
				scrollTop: offsetTop
			},{
				queue: false,
				duration: 1500, 
				easing: 'easeInOutCubic',
				complete: function(){
					$.cookie('jselpos',$('#select-menu').scrollTop(), { path: '/' });
				}
			});
	}else{
		$('#select-menu ul li a:first')
			.addClass('blue')
			.removeClass('red');
		$.cookie('jselpos', null);
	}
	
});
$(document).bind("dragstart", function() {
    return false;
});
function cancelClick(e){
	if(selectMenuMoving){
		e.stopPropagation();
		return false;
	}else{
		e.stopPropagation();
		$.cookie('jselpos',$('#select-menu').scrollTop(), { path: '/' });
		location.href = $(this).attr('href');
		return false;
	}
}