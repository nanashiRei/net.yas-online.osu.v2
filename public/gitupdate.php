<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_GET['key']) && hash('sha256',$_GET['key']) == '031f3c60f8608c368761357eab78d27dc7816ba6f821db93ee7b95a228cf5df4') {
        // Git PULL
        exec('/usr/bin/git pull > /dev/null 2>&1 &');
    } else {
        $f = fopen('auto-update.log','a');
        if($f){
            fputs($f,'Error 2: '.chr(10).var_export($_SERVER,true).chr(10).var_export($_GET,true).chr(10));
            fclose($f);
        }
    }
} else {
    $f = fopen('auto-update.log','a');
    if($f){
        fputs($f,'Error 1: '.chr(10).var_export($_SERVER,true).chr(10).var_export($_GET,true).chr(10));
        fclose($f);
    }
}
