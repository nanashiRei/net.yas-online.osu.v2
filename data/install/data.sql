INSERT INTO `uploader` (`id`,`name`,`lastupload`)
VALUES
('1', 'System', '2012-05-17 23:31:33');

INSERT INTO `files` (`id`,`filename`,`filesize`,`downloads`)
VALUES
('1', 'default_500.rar', '10277093376', '12'),
('2', '12 Map.osz', '10000000', '1'),
('3', '34 Map2.osz', '30000000', '2');
 
INSERT INTO `themes` (`id`,`name`,`path`,`numerical`)
VALUES
('1', 'Beatmap Pack', 'packs/default', '1');

INSERT INTO `maps` (`id`,`fileid`,`added`,`uploader`,`name`)
VALUES
('1', '2', '2012-05-18 03:45:37', '1', 'Map'),
('2', '3', '2012-05-18 03:45:37', '1', 'Map2');

INSERT INTO `packs` (`id`,`fileid`,`added`,`uploader`,`themeid`,`packname`)
VALUES
('1', '1', '2012-05-18 03:42:41', '1', '1', '500');

INSERT INTO `links` (mapid, packid)
VALUES
(1,1),
(2,1);
