delimiter $$

-- Tables
CREATE TABLE IF NOT EXISTS uploader (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(100) DEFAULT NULL,
    lastupload datetime DEFAULT NULL,
    PRIMARY KEY (id)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE IF NOT EXISTS files (
    id int(11) NOT NULL AUTO_INCREMENT,
    filename varchar(50) DEFAULT NULL,
    filesize int(11) DEFAULT NULL,
    downloads int(11) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY id (id)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE IF NOT EXISTS themes (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(45) DEFAULT NULL,
    path varchar(255) DEFAULT NULL,
    numerical int(11) DEFAULT '1',
    PRIMARY KEY (id)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE IF NOT EXISTS maps (
    id int(11) NOT NULL AUTO_INCREMENT,
    fileid int(11) DEFAULT NULL,
    added datetime DEFAULT NULL,
    uploader int(11) DEFAULT NULL,
    name varchar(100) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY maps_id_files (fileid),
    KEY maps_uploader (uploader),
    CONSTRAINT maps_id_files FOREIGN KEY (fileid)
        REFERENCES files (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT maps_uploader FOREIGN KEY (uploader)
        REFERENCES uploader (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE IF NOT EXISTS packs (
    id int(11) NOT NULL AUTO_INCREMENT,
    fileid int(11) DEFAULT NULL,
    added datetime DEFAULT NULL,
    uploader int(11) DEFAULT '1',
    themeid int(11) DEFAULT NULL,
    packname varchar(100) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY id (fileid),
    KEY id_files (fileid),
    KEY packs_id_themes (themeid),
    KEY packs_uploader (uploader),
    CONSTRAINT packs_uploader FOREIGN KEY (uploader)
        REFERENCES uploader (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT packs_id_files FOREIGN KEY (fileid)
        REFERENCES files (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT packs_id_themes FOREIGN KEY (themeid)
        REFERENCES themes (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE IF NOT EXISTS links (
    mapid int(11) DEFAULT NULL,
    packid int(11) DEFAULT NULL,
    KEY mapcontraints_id_maps (mapid),
    KEY mapconstraints_id_packs (packid),
    CONSTRAINT mapcontraints_id_maps FOREIGN KEY (mapid)
        REFERENCES maps (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT mapconstraints_id_packs FOREIGN KEY (packid)
        REFERENCES packs (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=InnoDB DEFAULT CHARSET=utf8$$

-- Views
CREATE VIEW beatmappack AS
SELECT  packs.id, 
        packs.added,
        packs.packname,
        files.filesize,
        files.filename,
        files.downloads,
        themes.path,
        themes.name,
        themes.numerical,
        uploader.name AS "uploader"
FROM    packs
LEFT JOIN files
ON files.id = packs.fileid
LEFT JOIN themes
ON themes.id = packs.themeid
LEFT JOIN uploader
ON uploader.id = packs.uploader$$
